// WDC028

//Data Persistence via Mongoose ODM
// -- Persist data via the use of Mongoose Object Document Mapper

// What is ODM?
// -- Object Document Mapper is a tool that translates object in code to documents to be used in document-based databases such as MongoDB

// What is Mongoose?
// -- An Object Data Modeling (ODM) library for MongoDB and Node.js. It manages relationships between data

// Schemas
// -- What is inside the document created by the collection
// -- An architecture or blueprint of how data will appear, a database schema describes the shape of the data and how it relates to other models, tables and databases.

// Models
// -- 


// "npm install express mongoose" to install both express and mongoose
// .gitignore the "node_modules"


const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Establish connection to mongoose database
mongoose.connect("mongodb+srv://admin:12345@cluster0.p2t9q.mongodb.net/b177-to-do?retryWrites=true&w=majority", {
// To avoid current and future errors in connection
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Set notification for connection success or failure
// Connection to the database
let db = mongoose.connection;

// If a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output a message in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Creat a Task Schema
const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		// Default values are the pre-defined values for a field
		default: "pending"
	}
})




// Create Models
// Server > Schema > Database > Collection(MongoDB)
const Task = mongoose.model("Task", taskSchema) // Task indicates the collection to be saved, taskSchema indicates the Schema above


app.use(express.json());

app.use(express.urlencoded({extended:true}));


// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		// If a document was found and the documents name matches the information sent via the client/postman
		if(result !=null && result.name == req.body.name) {
			// Returns a message to the client/postman
			return res.send("Duplicate task found")
		}
		// If no existing document found
		else{
			// Will create a new task, saving it into a variable and save it to the database
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error print
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// Create a GET request to retrieve all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		// If an error occured
		if(err){
			// Will print any errors found in the console
			return console.log(err);
		}
		// If no errors found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})




// 1. Create a User schema.
// 2. Create a User model.
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
// 5. Add a commit message Add activity code and push your project to gitlab.
// 6. Add the link in Boodle.
// *Stretch goal
//  - Create a GET route to view all the registered user
// Create a GET request to retrieve all the users

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found")
		}
		else{
			let newUser = new User({
				username: req.body.username,
				password : req.body.password
			})

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user created");
				}
			})
		}
	})
})

app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));

